const express = require("express");
const app = express();
const port = 3000; // Choose any port you prefer

app.get("/", function (req, res) {
  res.send("Hello America");
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
